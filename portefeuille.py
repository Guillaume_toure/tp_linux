class MontantInsuffisant(Exception):
    pass


class Portefeuille(object):

    def __init__(self, initial_amount=0):
        self.balance = initial_amount

    def depenser_argent(self, amount):
        if self.balance < amount:
            raise MontantInsuffisant('Pas assez de fonds disponibles {}'
                                     .format(amount))
        self.balance -= amount

    def ajouter_argent(self, amount):
        self.balance += amount
